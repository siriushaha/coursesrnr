import expect from 'expect';
import { getAuthorsFormattedForDropDown} from './CourseSelectors';

describe('Author Selectors', () => {
  describe('getAuthorsFormattedForDropDown', () => {
    it('should return author data formatted for use in drop down', () => {
      const authors = [
        {id: 'cory-house', firstName: 'Cory', lastName: 'House'},
        {id: 'steve-smith', firstName: 'Steve', lastName: 'Smith'}
      ];
      const expected = [
        {value: 'cory-house', text: 'Cory House'},
        {value: 'steve-smith', text: 'Steve Smith'}
      ];
      expect(getAuthorsFormattedForDropDown(authors)).toEqual(expected);
    });
  });
});
