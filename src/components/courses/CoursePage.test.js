import expect from 'expect';
import React from 'react';
import { mount, shallow } from 'enzyme';
import TestUtils from 'react-addons-test-utils';
import { CoursePage } from './CoursePage';
import CourseForm from './CourseForm';

function setup(saving=false) {
  const props = {
    course: {},
    saving: saving,
    authors: [],
    errors: {},
    onSave: () => {},
    onChange: () => {}
  };
  return shallow(<CourseForm {...props} />);
}

describe('CoursePage via Enzyme', () => {
  it('set error message when trying to save empty title', () => {
    const props = {
      authors: [],
      actions: { saveCourse: () => { return Promise.resolve();} },
      course: {id:'', watchHref:'', title:'', authorId:'',length:'', category:''}
    };
    const wrapper = mount(<CoursePage {...props}/>);
    const saveButton = wrapper.find('input').last();
    expect(saveButton.prop('type')).toBe('submit');
    saveButton.simulate('click');
    expect(wrapper.state().errors.title).toBe('Title must be at least 5 characters.');
  });

});
