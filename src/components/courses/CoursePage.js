import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import toastr from 'toastr';

import * as courseActions from '../../actions/CourseActions';
import CourseForm from './CourseForm';
import { getAuthorsFormattedForDropDown } from '../../selectors/CourseSelectors';


export class CoursePage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      course: Object.assign({}, this.props.course),
      errors: {},
      saving: false
    };
    this.updateCourse = this.updateCourse.bind(this);
    this.saveCourse = this.saveCourse.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.course.id != nextProps.course.id) {
      //Necessary to populate form when existing course is loaded directly
      this.setState({course: Object.assign({}, nextProps.course)});
    }
  }

  updateCourse(event) {
    const field = event.target.name;
    let course = this.state.course;
    course[field] = event.target.value;
    return this.setState({course: course});
  }

  isCourseFormValid() {
    let formValid = true;
    let errors = {};
    if (this.state.course.title.length < 5) {
      errors.title = 'Title must be at least 5 characters.';
      formValid = false;
    }
    this.setState({errors});
    return formValid;
  }

  saveCourse(event) {
    event.preventDefault();
    if (!this.isCourseFormValid()) return;
    this.setState({saving: true});
    this.props.actions.saveCourse(this.state.course)
      .then(() => this.redirect())
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      });
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Course saved.');
    this.context.router.push('/courses');
  }

  onCancel() {
    this.context.router.push('/courses');
  }
  render() {
      return (
        <CourseForm
          authors={this.props.authors}
          course={this.state.course}
          errors={this.state.errors}
          onSave={this.saveCourse}
          onChange={this.updateCourse}
          saving={this.state.saving}
        />
      );
  }
}

function getCourseById(courses, id) {
  let course = courses.find(course => course.id === id);
  return course;
}

function mapStateToProps(state, ownProps) {
  const courseId = ownProps.params.id;
  let course = {id:'', watchHref:'', title:'', authorId:'',length:'', category:''};
  if (courseId && state.courses.length > 0)
    course = getCourseById(state.courses, courseId);

  const authorsFormattedForDropDown = getAuthorsFormattedForDropDown(state.authors);

  return {
    course: course,
    authors: authorsFormattedForDropDown
  };
}

function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(courseActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CoursePage);

CoursePage.propTypes = {
  actions: PropTypes.object.isRequired,
  authors: PropTypes.array.isRequired,
  course: PropTypes.object.isRequired
};

CoursePage.contextTypes = {
  router: PropTypes.object.isRequired
};
