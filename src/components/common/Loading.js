/*eslint-disable react/no-did-mount-set-state */
import React, {Component, PropTypes} from 'react';

class Loading extends Component
{
  constructor(props, context)
  {
    super(props, context);
    this.state = { frame: 1};
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({
        frame: this.state.frame + 1
      });
    }, this.props.interval);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render()
  {
    let dots = this.state.frame % (this.props.dots + 1);
    let text = '';
    while (dots > 0) {
      text += '.';
      dots--;
    }
    return (
      <span {...this.props}>{text}&nbsp;</span>
    );
  }
}

Loading.propTypes = {
  interval: PropTypes.number,
  dots: PropTypes.number
};

Loading.defaultTypes = {
  interval: 300,
  dots: 3
};

export default Loading;
