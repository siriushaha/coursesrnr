import expect from 'expect';
import { createStore } from 'redux';
import rootReducer from '../reducers';
import initialState from '../reducers/InitialState';
import * as courseActions from '../actions/CourseActions';

describe('Store', () => {
  it('should handle course creation', () => {
    //arrange
    const store = createStore(rootReducer, initialState);
    const course = { title: 'My ABC exam'};

    //act
    const action = courseActions.createCourseSuccess(course);
    store.dispatch(action);

    //assert
    const actual = store.getState().courses[0];
    const expected = { title: 'My ABC exam'};
    expect(actual).toEqual(expected);
  });
});
