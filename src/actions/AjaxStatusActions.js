import * as ajaxTypes from '../constants/AjaxConstants';

export const beginAjaxCall = () => {
  return { type: ajaxTypes.BEGIN_AJAX_CALL };
};

export const ajaxCallError = (error) => {
  return { type: ajaxTypes.AJAX_CALL_ERROR };
};
