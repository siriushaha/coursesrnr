import * as authorTypes  from './../constants/AuthorConstants';
import authorService from '../services/AuthorService';
import { beginAjaxCall } from './AjaxStatusActions';

export const loadAuthors = () => {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return authorService.getAuthors()
      .then(authors => {
        dispatch(loadAuthorsSuccess(authors));
      })
      .catch(error => {
        throw(error);
      });
  };
};

export const loadAuthorsSuccess = (authors) => {
  //debugger;
  return {
    type: authorTypes.LOAD_AUTHORS_SUCCESS,
    authors
  };
};
