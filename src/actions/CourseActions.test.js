import expect from 'expect';
import * as courseActions from './CourseActions';
import * as courseTypes from '../constants/CourseConstants';
import * as ajaxTypes from '../constants/AjaxConstants';

import thunk from 'redux-thunk';
import nock from 'nock';
import configureMockStore from 'redux-mock-store';

describe('Course Actions', () => {
  describe('createCourseSuccess', () => {
    it('should create a CREATE_COURSE_SUCCESS action', () => {
      const course = {id: 'clean-code', title: 'Clean Code'};
      const expectedAction = {
        type: courseTypes.CREATE_COURSE_SUCCESS,
        course
      };

      const action = courseActions.createCourseSuccess(course);
      expect(action).toEqual(expectedAction);
    });
  });
});

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Async Actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should create BEGIN_AJAX_CALL and LOAD_COURSES_SUCCESS when loading courses', (done) => {
    // Example call to nock
    // nock('http://eample.com)
    //      .get('/courses')
    //      .reply(200, {body: { courses: [{id:1, firstName: 'Steve', lastName: 'Smith'}]}})

    const expectedActions = [
      {type: ajaxTypes.BEGIN_AJAX_CALL},
      {type: courseTypes.LOAD_COURSES_SUCCESS, body: { courses: [{id:1, firstName: 'Steve', lastName: 'Smith'}]}}
    ];

    const store = mockStore({courses: []}, expectedActions);
    store.dispatch(courseActions.loadCourses())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0].type).toEqual(ajaxTypes.BEGIN_AJAX_CALL);
        expect(actions[1].type).toEqual(courseTypes.LOAD_COURSES_SUCCESS);
        done();
      });
  });
});
