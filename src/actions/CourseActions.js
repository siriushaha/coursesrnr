import * as courseTypes  from './../constants/CourseConstants';
import courseService from '../services/CourseService';
import { beginAjaxCall, ajaxCallError } from './AjaxStatusActions';

export const loadCourses = () => {
  return (dispatch) => {
    dispatch(beginAjaxCall());
    return courseService.getCourses()
      .then(courses => {
        dispatch(loadCoursesSuccess(courses));
      })
      .catch(error => {
        throw(error);
      });
  };
};

export const loadCoursesSuccess = (courses) => {
  //debugger;
  return {
    type: courseTypes.LOAD_COURSES_SUCCESS,
    courses
  };
};

export const saveCourse = (course) => {
  return (dispatch, getState) => {
    dispatch(beginAjaxCall());
    return courseService.saveCourse(course)
      .then(course => {
        course.id ? dispatch(updateCourseSuccess(course)) : dispatch(createCourseSuccess(course));
      })
      .catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
  };
};

export const updateCourseSuccess = (course) => {
  //debugger;
  return {
    type: courseTypes.UPDATE_COURSE_SUCCESS,
    course
  };
};

export const createCourseSuccess = (course) => {
  //debugger;
  return {
    type: courseTypes.CREATE_COURSE_SUCCESS,
    course
  };
};
