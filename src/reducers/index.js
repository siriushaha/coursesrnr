import { combineReducers } from 'redux';
import courseReducer from './CourseReducer';
import authorReducer from './AuthorReducer';
import ajaxStatusReducer from './AjaxStatusReducer';

const rootReducer = combineReducers({
  courses: courseReducer,
  authors: authorReducer,
  ajaxCallsInProgress: ajaxStatusReducer
  }
);

export default rootReducer;
