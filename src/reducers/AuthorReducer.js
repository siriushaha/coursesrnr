import * as authorTypes from '../constants/AuthorConstants';
import initialState from './InitialState';

const authorReducer = (state=initialState.authors , action) => {
  switch (action.type) {
    case authorTypes.LOAD_AUTHORS_SUCCESS:
      //debugger;
      return action.authors;

    default:
      return state;
  }

};

export default authorReducer;
