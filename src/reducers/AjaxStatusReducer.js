import * as ajaxTypes from '../constants/AjaxConstants';
import initialState from './InitialState';

const ajaxStatusReducer = (state = initialState.ajaxCallsInProgress, action) => {
  if (action.type === ajaxTypes.BEGIN_AJAX_CALL) {
    return state + 1;
  }
  else if (action.type == ajaxTypes.AJAX_CALL_ERROR || actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }
  return state;
};

const actionTypeEndsInSuccess = (actionType) => {
  return actionType.endsWith('_SUCCESS');
};

export default ajaxStatusReducer;
