import * as courseTypes from '../constants/CourseConstants';
import initialState from './InitialState';

const courseReducer = (state=initialState.courses, action) => {
  switch (action.type) {
    case courseTypes.LOAD_COURSES_SUCCESS:
          //debugger;
          return action.courses;

    case courseTypes.CREATE_COURSE_SUCCESS:
      //debugger;
      return [...state, Object.assign({}, action.course)];

    case courseTypes.UPDATE_COURSE_SUCCESS:
      //debugger;
      return [...state.filter(course => course.id !== action.course.id), Object.assign({}, action.course)];
    
    default:
          return state;
  }

};

export default courseReducer;
